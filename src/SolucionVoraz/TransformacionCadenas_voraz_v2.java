/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SolucionVoraz;

/**
 *
 * @author lambda
 */
public class TransformacionCadenas_voraz_v2 {

    private String cadenaA;
    private String cadenaB;

    private int longitudA;
    private int longitudB;

    private int arrayCalculos[];

    public TransformacionCadenas_voraz_v2(String cadenaA, String cadenaB) {
        this.cadenaA = cadenaA;
        this.cadenaB = cadenaB;

        this.longitudA = cadenaA.length();
        this.longitudB = cadenaB.length();

    }

    public int[] getMatrizCalculos() {
        return arrayCalculos;
    }

    public String getCadenaA() {
        return cadenaA;
    }

    public String getCadenaB() {
        return cadenaB;
    }

    public int getLongitudA() {
        return longitudA;
    }

    public int getLongitudB() {
        return longitudB;
    }
    
    
    

    public int calcularTransformaciones() {
        int numero_transformaciones = 0;

        //------------------------------Cadenas Iguales--------------------
        if (longitudA == longitudB) {

            char arrayCadenaA[] = cadenaA.toCharArray();
            char arrayCadenaB[] = cadenaB.toCharArray();

            arrayCalculos = new int[longitudA + 1];

            //Siempre en 0,0 = 0 (vacio con vacio)
            arrayCalculos[0] = 0;

            for (int i = 1; i <= longitudB; i++) {

                if (arrayCadenaA[i - 1] != arrayCadenaB[i - 1]) {

                    arrayCalculos[i] = arrayCalculos[i - 1] + 1;

                    System.out.println("Reemplazo en " + (i - 1));

                } else {
                    arrayCalculos[i] = arrayCalculos[i - 1] + 0;

                    System.out.println("No hago nada en " + (i - 1));

                }

            }
        }

        //--------------------------------------------------
        //------------------------------Cadena A > B--------------------
        if (longitudA > longitudB) {
            arrayCalculos = new int[longitudA + 1];

            //Completar la cadena B con espacios
            for (int i = longitudB - 1; i < longitudA - 1; i++) {
                cadenaB += " ";
            }

            longitudB = longitudA;
            
            char arrayCadenaA[] = cadenaA.toCharArray();
            char arrayCadenaB[] = cadenaB.toCharArray();

            for (int i = 1; i <= longitudA; i++) {

                if (arrayCadenaB[i - 1] == ' ') {//Como complete con espacios 

                    arrayCalculos[i] = arrayCalculos[i - 1] + 1;//eliminar en A
                    System.out.println("Elimino en " + (i - 1));

                } else if (arrayCadenaA[i - 1] != arrayCadenaB[i - 1]) {

                    arrayCalculos[i] = arrayCalculos[i - 1] + 1;

                    System.out.println("Reemplazo en " + (i - 1));

                } else {
                    arrayCalculos[i] = arrayCalculos[i - 1] + 0;

                    System.out.println("No hago nada en " + (i - 1));

                }

            }

        }

        //--------------------------------------------------
        //------------------------------Cadena B > A--------------------
        if (longitudA < longitudB) {
            arrayCalculos = new int[longitudB + 1];

            //Completar la cadena B con espacios
            for (int i = longitudA - 1; i < longitudB - 1; i++) {
                cadenaA += " ";
            }
            
            longitudA = longitudB;

            char arrayCadenaA[] = cadenaA.toCharArray();
            char arrayCadenaB[] = cadenaB.toCharArray();

            for (int i = 1; i <= longitudB; i++) {

                if (arrayCadenaA[i - 1] == ' ') {//Como complete con espacios, si hay espacio, indica que debe adicionarse

                    arrayCalculos[i]= arrayCalculos[i - 1]+ 1;//Adicionar en A
                    System.out.println("Adicion en " + (i - 1));

                } else if (arrayCadenaA[i - 1] != arrayCadenaB[i - 1]) {

                    arrayCalculos[i] = arrayCalculos[i - 1] + 1;

                    System.out.println("Reemplazo en " + (i - 1));

                } else {
                    arrayCalculos[i] = arrayCalculos[i - 1] + 0;

                    System.out.println("No hago nada en " + (i - 1));
                }

            }

        }

        numero_transformaciones = arrayCalculos[arrayCalculos.length - 1];

        return numero_transformaciones;

    }
    
    public static void main(String args[])
    {
        
        TransformacionCadenas_voraz_v2 obj = new TransformacionCadenas_voraz_v2("bab","ab");
        System.out.println("Número de operaciones: "+obj.calcularTransformaciones());
        
    }
    
}
    
    





