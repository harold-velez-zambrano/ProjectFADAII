/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SolucionDinamica;

/**
 *
 * @author lambdavelez
 */
public class TransformacionCadenas_Dinamica {

    private String cadenaA;
    private String cadenaB;

    private int longitudA;
    private int longitudB;

    private int matrizCalculos[][];

    public TransformacionCadenas_Dinamica(String cadenaA, String cadenaB) {
        this.cadenaA = cadenaA;
        this.cadenaB = cadenaB;

        this.longitudA = cadenaA.length();
        this.longitudB = cadenaB.length();

        //Filas representadas por la cadena A, columnas por la cadena B
        matrizCalculos = new int[cadenaA.length() + 1][cadenaB.length() + 1];
    }

    //--------------Métodos get-----------------//
    public String getCadenaA() {
        return cadenaA;
    }

    public String getCadenaB() {
        return cadenaB;
    }

    public int getLongitudA() {
        return longitudA;
    }

    public int getLongitudB() {
        return longitudB;
    }

    public int[][] getMatrizCalculos() {
        return matrizCalculos;
    }

    //-------------Métodos set------------------//
    public void setCadenaA(String cadenaA) {
        this.cadenaA = cadenaA;
    }

    public void setCadenaB(String cadenaB) {
        this.cadenaB = cadenaB;
    }

    public void setLongitudA(int longitudA) {
        this.longitudA = longitudA;
    }

    public void setLongitudB(int longitudB) {
        this.longitudB = longitudB;
    }

    public void setMatrizCalculos(int[][] matrizCalculos) {
        this.matrizCalculos = matrizCalculos;
    }

    /**
     *
     * @param a
     * @param b
     * @param c
     * @return el menor entre los tres números.
     */
    public int minimo(int a, int b, int c) {

        return Math.min(a, Math.min(b, c));

    }

    public int calcularTransformaciones() {

        char arrayCadenaA[] = cadenaA.toCharArray();
        char arrayCadenaB[] = cadenaB.toCharArray();

        //Enteros auxiliares que almacenan el valor de ciertas posiciones de la matrizCalculos
        int a, b, c;

        //+---------Casos triviales ----------+
        //
        //Número de operaciones entre dos cadenas vacías es 0
        matrizCalculos[0][0] = 0;

        //Cuando la cadena B es vacia
        for (int i = 1; i <= arrayCadenaA.length; i++) {
            matrizCalculos[i][0] = i;
        }

        //Cuando la cadena A es vacía
        for (int j = 1; j <= arrayCadenaB.length; j++) {
            matrizCalculos[0][j] = j;
        }

        //+------Caso no trivial-----------+
        for (int i = 1; i < cadenaA.length() + 1; i++) {
            for (int j = 1; j < cadenaB.length() + 1; j++) {

                if (arrayCadenaA[i - 1] != arrayCadenaB[j - 1]) {
                    a = matrizCalculos[i - 1][j - 1] + 1;//No son iguales, set
                } else {
                    a = matrizCalculos[i - 1][j - 1];
                }

                b = matrizCalculos[i][j - 1] + 1;
                c = matrizCalculos[i - 1][j] + 1;

                matrizCalculos[i][j] = minimo(a, b, c);

            }

        }

        return matrizCalculos[matrizCalculos.length - 1][matrizCalculos[0].length - 1];

    }

}
