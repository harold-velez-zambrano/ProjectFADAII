/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author lambdavelez
 */
public class ManejadorEventos implements ActionListener {
    
    private int fila;
    private int columna;
    private GUI gui;
    
    public ManejadorEventos(GUI in, int a, int b) {
        
        this.fila = a;
        this.columna = b;
        this.gui = in;
        
    }
    
    public ManejadorEventos(GUI gui) {
        this.gui = gui;
    }
    
    
    @Override
    public void actionPerformed(ActionEvent ae) {
        
        if (gui.salir == ae.getSource()) {
            System.exit(0);
        }
        
        gui.getBoton()[fila][columna].setText(String.valueOf(gui.getMatriz()[fila][columna]));
        
    }
    
}
