package GUI;

import SolucionDinamica.TransformacionCadenas_Dinamica;
import SolucionVoraz.TransformacionCadenas_voraz;
/**
 *
 * @author lambdavelez
 */

import javax.swing.*;
import java.awt.*;

public class GUI extends JFrame {

    private int filas;
    private int columnas;
    private int[][] matriz;
    private JButton boton[][];
    private TransformacionCadenas_Dinamica obj;
    private TransformacionCadenas_voraz obj2;
    //----------------------------------------

    JMenuBar Barra = new JMenuBar();
    JMenu Archivo = new JMenu("Archivo");
    JMenuItem salir = new JMenuItem("Salir");
    JLabel Nombre;

    public GUI(String tipo, String cadenaA, String cadenaB) {

        if (tipo.equals("Dinamica")) {
            this.obj = new TransformacionCadenas_Dinamica(cadenaA, cadenaB);
            obj.calcularTransformaciones();
            this.filas = obj.getLongitudA() + 1;
            this.columnas = obj.getLongitudB() + 1;
            matriz = obj.getMatrizCalculos();

            //------Si se elige voraz
        } else {
            this.obj2 = new TransformacionCadenas_voraz(cadenaA, cadenaB);
            obj2.calcularTransformaciones();
            this.filas = obj2.getLongitudA() + 1;
            this.columnas = obj2.getLongitudB() + 1;
            matriz = obj2.getMatrizCalculos();

        }

        Nombre = new JLabel("Solución " + tipo, JLabel.CENTER);

        boton = new JButton[this.filas][this.columnas];

        //menu
        Archivo.addSeparator();
        salir.addActionListener(new ManejadorEventos(this));
        Archivo.add(salir);
        Barra.add(Archivo);
        setJMenuBar(Barra);

        //Panel Principal 
        JPanel Principal = new JPanel();
        Principal.setLayout(new GridLayout(this.filas, this.columnas));

        //Colocar Botones
        for (int i = 0; i < this.filas; i++) {
            for (int j = 0; j < this.columnas; j++) {

                boton[i][j] = new JButton();
                boton[i][j].addActionListener(new ManejadorEventos(this, i, j));
                //-----Colores-----------------------
                boton[i][j].setBackground(Color.white);
                boton[i][j].setForeground(Color.BLACK);
                //------------------------------------  
                Principal.add(boton[i][j]);
            }

        }

        initMatriz();

        //---Especificaciones del frame----
        Nombre.setForeground(Color.BLUE);
        add(Nombre, "North");
        add(Principal, "Center");

        setLocation(170, 25);
        setSize(600, 600);
        setResizable(true);
        setTitle("FADA");
        setVisible(true);

    }

    public int[][] getMatriz() {
        return matriz;
    }

    public JButton[][] getBoton() {
        return boton;
    }

    public void initMatriz() {
        //-----------Mostrar las condiciones triviales --------

        for (int i = 0; i < this.filas; i++) {

            boton[i][0].setText(String.valueOf(matriz[i][0]));

        }

        for (int j = 0; j < this.columnas; j++) {

            boton[0][j].setText(String.valueOf(matriz[0][j]));

        }

        //--------------------------------------------------
    }

}
